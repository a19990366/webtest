package controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String showPage() {
		return "index";
	}
	
	@RequestMapping("/editormdPic")
	@ResponseBody
	public JSONObject editormdPic (@RequestParam(value = "editormd-image-file", required = true) MultipartFile file, HttpServletRequest request,HttpServletResponse response) throws Exception{

	        String trueFileName = file.getOriginalFilename();  

	        String suffix = trueFileName.substring(trueFileName.lastIndexOf("."));

	        String fileName = System.currentTimeMillis()+"_"+CommonUtils.getRandomNumber(100, 999)+suffix;  

	        String path = request.getSession().getServletContext().getRealPath("/assets/msg/upload/");
	        System.out.println(path);  

	        File targetFile = new File(path, fileName);  
	        if(!targetFile.exists()){  
	           targetFile.mkdirs();  
	        }  

	       //儲存  
	        try {  
	           file.transferTo(targetFile);  
	        } catch (Exception e) {  
	           e.printStackTrace();  
	        }  


	        JSONObject res = new JSONObject();
	        res.put("url", Constant.WEB_ROOT+"assets/msg/upload/"+fileName);
	        res.put("success", 1);
	        res.put("message", "upload success!");

	        return res;

	    }
}
