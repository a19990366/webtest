<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

	<head>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/editormd.css" />" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/editor-md/1.5.0/editormd.min.js"></script>
	<style type="text/css">
    #mynetwork {
      width: 600px;
      height: 400px;
      border: 1px solid lightgray;
    }
  	</style>
	</head>
	<body>
	
		<h2>
			Markdown test
		</h2>

		<script type="text/javascript" src="<c:url value="/resources/js/network.js" />"></script>

		<div id="my-editormd" >
		<textarea id="my-editormd-markdown-doc" name="my-editormd-markdown-doc" style="display:none;"></textarea>
		<!-- 注意：name屬性的值-->
		<textarea id="my-editormd-html-code" name="my-editormd-html-code" style="display:none;"></textarea>
		</div>
		
		<script type="text/javascript">
		$(function() {
		    editormd("my-editormd", {//注意1：這裡的就是上面的DIV的id屬性值
		        width   : "80%",
		        height  : 640,
		        syncScrolling : "single",
		        emoji : false,
		        path    : "resources/editormd/lib/" ,//注意2：你的路徑
		        saveHTMLToTextarea : true,//注意3：這個配置，方便post提交表單
		        
		        imageUpload : true,
		        imageFormats : ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
		        imageUploadURL : "/markmdtest/editormdPic"
		    });
		});
		</script>
		
		<script src="<c:url value="/resources/editormd/languages/zh-tw.js" />"></script>
	</body>

</html>